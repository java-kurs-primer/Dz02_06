# Dz02_06

Potrebno je napisati klasu `WordCountImpl` koja implementira interfejs `IWordCount`.  
```java
public interface IWordCount {

	public void analyze();

	default public String statistics() {
		return this.toString();
	}

	default public int getCountWord(String word) {
		return 0;
	}

	default public int getCountLetter(char letter) {
		return 0;
	}
}
```

## Uraditi
Potrebno je da klasa `WordCountImpl` ima javni konstruktor koji kao parametar prihvata teks u kome želimo da prebrojimo slova i reči. Ovaj tekst čuvati u odgovarajućoj promenljivoj.  
Prilikom konstruisanja novog objekta ne treba vršiti obradu dobijenog teksta, već samo sačuvati tekst u odgovarajuću promenljivu.  

Potrebno je implementirati sledeće metode iz interfejsa
Metoda `public void analyze()` vrši analizu teksta (prebrojava reči i slova u tekstu).

Metod `int getCountWord(String word)` vraća broj pojavljivanja reči `word` u tekstu.

Metoda `int getCountLetter(char letter)` vraća broj pojavljivanja slova `letter` u tekstu.

Metoda `String statistics()` vraća izveštaj u sledećem formatu:
```text
Statistika
Broj reči u tekstu [BROJ_REČI]
BROJ slova u tekstu [BROJ_SLOVA]
[REČ_0] [BROJ_0]
[REČ_1] [BROJ_1]
[...] [...]
[REČ_N] [BROJ_N]
```
Značenje promenljivih
* [BROJ_REČI] - ukupan broj reči u tekstu
* [BROJ_SLOVA] - ukupan broj svih slova
* [REČ_0] - Prva reč
* [BROJ_0] - Broj pojavljivanje [REČ_0]
* itd

## Napomena
U implementaciju interfejsa dodati sve potrebne promenljive i metode sa pravima pristupa private.

## Pretpostavke
Uvesti sledeće pretpostavke
* Prilikom pravljenja novog objekta `WordCountImpl` konstruktoru će biti prosleđen ispravan tekst.
* Tekst će sadrži samo reči i razmake. Tekst neće sadržati brojeve, znakove interpunkcije, niti specijalne znakove.
* Slova u tekstu će biti mala i velika slova engleskog alfabeta.
* U tekstu za analizu biće prosleđena samo mala slova.
* Prilikom poziva metode `int getCountWord(String word)` biće prosleđena reč koja može da sadrži mala i velika slova.
* Prilikom poziva metode `int getCountLetter(char letter)` biće prosleđena samo mala ili velika slova.

## Razmisliti
Kako biste napisali metodu `statistics()` tako da pri ispisu reči budu sortirane po abecednom redu?  
A kako da budu sortirane po broju pojavljivanja u kodu?
