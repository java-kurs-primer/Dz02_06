package rs.ac.singidunum.test.dz03_06;

import org.junit.Assert;
import org.junit.Test;
import rs.ac.singidunum.dz03_06.IWordCount;
import rs.ac.singidunum.dz03_06.WordCountImpl;

public class Tests {

  @Test
  public void test01() {
    String text = "count words in a text";
    IWordCount wc = new WordCountImpl(text);
    wc.analyze();

    Assert.assertEquals(1, wc.getCountWord("count"));
    Assert.assertEquals(0, wc.getCountWord("some"));
    Assert.assertEquals(1, wc.getCountWord("COUNT"));
  }

  @Test
  // Test if method analyze is called
  public void test02() {
    String text = "count words in a text, when the method analyze is not called";
    IWordCount wc = new WordCountImpl(text);

    Assert.assertEquals(0, wc.getCountWord("aaa"));
    Assert.assertEquals(0, wc.getCountWord("count"));
    Assert.assertEquals(0, wc.getCountLetter('a'));

    wc.analyze();
    Assert.assertEquals(0, wc.getCountWord("aaa"));
    Assert.assertEquals(1, wc.getCountWord("count"));
    Assert.assertEquals(10, wc.getCountLetter('a'));
  }
}
