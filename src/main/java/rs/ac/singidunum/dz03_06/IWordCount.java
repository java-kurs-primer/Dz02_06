package rs.ac.singidunum.dz03_06;

public interface IWordCount {

	public void analyze();

	default public String statistics() {
		return this.toString();
	}

	default public int getCountWord(String word) {
		return 0;
	}

	default public int getCountLetter(char letter) {
		return 0;
	}
}
